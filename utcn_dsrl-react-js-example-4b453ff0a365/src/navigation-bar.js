import React from 'react'
import logo from './commons/images/icon.png';

import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

const NavigationBar = () => (
    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"35"} />
            </NavbarBrand>
            <Nav className="mr-auto" navbar>

                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle style={textStyle} nav caret>
                       Menu
                    </DropdownToggle>

                    <DropdownMenu right >

                        <DropdownItem>
                            <NavLink href="/doctor">Doctor</NavLink>
                            <NavLink href="/pacienti">Pacienti</NavLink>
                            <NavLink href="/caregivers">Asistenti</NavLink>
                        </DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>
            </Nav>

            <Nav className="mr-auto" navbar>

                <UncontrolledDropdown nav inNavbar>

                    <DropdownToggle style={textStyle} nav caret>
                        Login/Register
                    </DropdownToggle>
                    <DropdownMenu right >

                        <DropdownItem>
                            <NavLink href="/login">Login</NavLink>
                            <NavLink href="/register">Register</NavLink>
                        </DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>

            </Nav>
        </Navbar>
    </div>
);

export default NavigationBar
