import React from "react";
import { Link } from 'react-router-dom';

class Login extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="card" ref={this.props.containerRef}>
                <div className="content">
                    <div className="form">
                        <div className="form-group">
                            <label htmlFor="username">Username</label>
                            <input type="text" class="form-control" name="username" placeholder="username" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <input type="text" name="email" class="form-control"  placeholder="email" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="password" class="">Password</label>
                            <input type="text" name="password" class="form-control"  placeholder="password" />
                        </div>
                    </div>
                    <div className="form-group form-check">
                        <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
                        <label className="form-check-label" htmlFor="exampleCheck1">Don't forget me</label>
                    </div>
                </div>

                <div className="footer">
                    <Link to="/caregivers">
                        <button class="btn btn-danger btn-lg btn-block">
                            Register!
                        </button>
                    </Link>
                </div>
            </div>
        );
    }
}

export default Login;