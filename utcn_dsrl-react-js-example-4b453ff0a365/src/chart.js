import React, {Component} from 'react';
import {Bar,Polar,Pie, Bubble, Radar, Line,Scatter, HorizontalBar,Doughnut} from 'react-chartjs-2';

class Chart extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            chartData:props.chartData
        }
    }

    static defaultProps = {
        displayTitle:true,
        displayLegend: true,
        legendPosition:'right',
        location:'City'
    }

    render(){
        return (
            <div className="chart">
                <Line
                    data={this.state.chartData}
                    options={{
                        title:{
                            display:this.props.displayTitle,
                            text:'Activities',
                            fontSize:20
                        },
                        legend:{
                            display:this.props.displayLegend,
                            position:this.props.legendPosition
                        }
                    }}
                />


            </div>

        )
    }
}
export default Chart;