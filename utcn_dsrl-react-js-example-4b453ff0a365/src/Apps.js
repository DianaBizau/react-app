import React, { Component } from 'react';

import './Apps.css';
import Chart from './chart';

class Apps extends Component {
    constructor(){
        super();
        this.state = {
            chartData:{}
        }
    }

    componentWillMount(){
        this.getChartData();
    }

    getChartData(){
        // Ajax calls here
        this.setState({
            chartData:{
                labels: ['Eating', 'Sleeping', 'Grooming', 'Leaving', 'Toaleting', 'Speare_Time'],
                datasets:[
                    {
                        label:'Population',
                        data:[
                            30,
                            50,
                            15,
                            10,
                            40,
                            9
                        ],
                        backgroundColor:[
                            'rgba(255, 99, 132, 0.6)',
                            'rgba(54, 162, 235, 0.6)',
                            'rgba(255, 206, 86, 0.6)',
                            'rgba(75, 192, 192, 0.6)',
                            'rgba(153, 102, 255, 0.6)',
                            'rgba(255, 159, 64, 0.6)',
                            'rgba(255, 99, 132, 0.6)'
                        ]
                    }
                ]
            }
        });
    }

    render() {
        return (
            <div className="Apps">
                <div className="Apps-header">

                    <h2>Welcome to React</h2>
                </div>
                <Chart chartData={this.state.chartData} location="Massachusetts" legendPosition="bottom"/>
            </div>
        );
    }
}

export default Apps;
