import React,{Component} from 'react';
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css"
import {FormGroup} from "react-bootstrap";
import{Container,Input,Button,Label,Form,Table} from 'reactstrap'
import {Link} from 'react-router-dom'
import Moment from 'react-moment';
import {HOST} from "./commons/hosts";
import './Apps.css';

import RestApiClient from "./commons/api/rest-client";
import * as API_USERS from "./person-data/person/api/person-api";
import Chart from "./chart";
const endpoint = {
    get_pacienti: '/doctor/',
    post_pacienti: '/doctor/createPacient'
};
function postPerson(user, callback){
    let request = new Request(HOST.backend_api + endpoint.post_pacienti , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}
class Doctor extends React.Component{

    registerPcienti(pacienti){
        return API_USERS.postPerson(pacienti, (result, status, error) => {
            console.log(result);

            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully inserted person with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }
    // String name=asistent.getName();
    // String username=asistent.getUsername();
    // String password=asistent.getPassword();
    // String adresa = asistent.getAddress();
    // String gen=asistent.getGen();
    // LocalDateTime date=asistent.getDate();
    emptyItem={
        name:'',
        username:'',
        password:'',
        adresa:'',
        gen:'',
        date: new Date()
    }



    componentWillMount(){
        this.getChartData();
    }

    getChartData(){
        // Ajax calls here
        this.setState({
            chartData:{
                labels: ['Eating', 'Sleeping', 'Grooming', 'Leaving', 'Toaleting', 'Speare_Time'],
                datasets:[
                    {
                        label:'Population',
                        data:[
                            30,
                            50,
                            15,
                            10,
                            40,
                            9
                        ],
                        backgroundColor:[
                            'rgba(255, 99, 132, 0.6)',
                            'rgba(54, 162, 235, 0.6)',
                            'rgba(255, 206, 86, 0.6)',
                            'rgba(75, 192, 192, 0.6)',
                            'rgba(153, 102, 255, 0.6)',
                            'rgba(255, 159, 64, 0.6)',
                            'rgba(255, 99, 132, 0.6)'
                        ]
                    }
                ]
            }
        });
    }

    constructor(props){
        super(props);

        this.state={timerStarted: false,
            timerStopped: true,
            hours: 0,
            minutes: 0,
            seconds: 0,
            captures: [],
            date: new Date(),
            isLoading:true,
            Doctors:[],
            Pacienti:[],
            Asistents:[],
            item : this.emptyItem,
            chartData:{}
        }
        this.handleSubmit=this.handleSubmit.bind(this);
        this.handleChange=this.handleChange.bind(this);
        this.handleDateChange=this.handleDateChange.bind(this);
        this.tableData = [];
    }

    async componentDidMount(){
        const response=await fetch('/doctor/findAll');
        const body = await response.json();
        this.setState({Doctors :body, isLoading: false});

        const responsed=await fetch('/App/getPacienti');
        const bodyd = await responsed.json();
        this.setState({Pacienti :bodyd, isLoading: false});

        const responseA=await fetch('/Asistent/getAsistenti');
        const bodyA = await responseA.json();
        this.setState({Asistents :bodyA, isLoading: false});
    }

    async remove(id) {
         await fetch('/doctor/remove/${id}', {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(() => {
            let updatePacienti = [...this.state.Pacienti].filter(
                i => i.id !== id);
            this.setState({Pacienti: updatePacienti});
        });
    }
    async removeAsistenti(id) {
        await fetch('/doctor/delete/{id}', {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(() => {
            let updateAsistenti = [...this.state.Asistents].filter(
                i => i.id !== id);
            this.setState({Asistents: updateAsistenti});
        });
    }
    handleTimerStop() {
        this.setState({timerStarted: false, timerStopped: true});
        clearInterval(this.timer);
    }
    handleTimerStart(e) {
        e.preventDefault();
        if(this.state.timerStopped) {
            this.timer = setInterval(() => {
                this.setState({timerStarted: true, timerStopped: false});
                if(this.state.timerStarted) {
                    if(this.state.seconds >= 60) {
                        this.setState((prevState) => ({ minutes: prevState.minutes + 1, seconds: 0}));
                    }
                    if(this.state.minutes >= 60) {
                        this.setState((prevState) => ({ hours: prevState.hours + 1, minutes: 0, seconds: 0}));

                    }
                    this.setState((prevState) => ({ seconds: prevState.seconds + 1 }));
                }

            }, 1000);
        }
    }


    async handleSubmit(event){
        event.preventDefault();
        const {item}=this.state;
        await fetch ("/doctor/createPacient", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({item}),
        });
        console.log(this.state);
        this.props.history.push('/doctor');
        console.log("is added");
        //this.registerPacienti(pacienti);
    }
    handleChange(event){
        const target= event.target;
        const value= target.value;
        const name=target.name;
        let item={...this.state.item};
        item[name]=value;
        this.setState({item})
        console.log(this.state.item)
    }
    handleDateChange(date){
        let item={...this.state.item};
        item.pacientdate=date;
        this.setState({item});
    }

    downloadEmployeeData = () => {
        fetch('http://localhost:8080/App/getPacienti')
            .then(response => {
                response.blob().then(blob => {
                    let url = window.URL.createObjectURL(blob);
                    let a = document.createElement('a');
                    a.href = url;
                    a.download = 'med.json';
                    a.click();
                });
                //window.location.href = response.url;
            });
    }

    render(){
        const{Doctors}=this.state;
        const{Asistents}=this.state;
        const{Pacienti,isLoading}=this.state;
        const title=<h3>Add Pacient/Asistent</h3>

        if(isLoading)
            return (<div>Loading ... </div>);
        let rows=
            Pacienti.map(pacienti=>
            <tr key={pacienti.id}>
                <td>{pacienti.name}</td>
                <td>{pacienti.username}</td>
                <td>{pacienti.password}</td>
                <td>{pacienti.address}</td>
                <td>{pacienti.gen}</td>
                <td><Moment date={pacienti.data} format = "YYYY/MM/DD"/></td>

                <td><Button size="sm" color="danger" onClick={()=>this.remove(pacienti.id)} >Delete</Button></td>
                <td><Button size="sm" color="primary" >Add Medication plan</Button> </td>
            </tr>
            )
        let rowss=
            Asistents.map(asistenti=>
                <tr key={asistenti.id}>
                    <td>{asistenti.name}</td>
                    <td>{asistenti.username}</td>
                    <td>{asistenti.password}</td>
                    <td>{asistenti.address}</td>
                    <td>{asistenti.gen}</td>
                    <td><Moment date={asistenti.data} format = "YYYY/MM/DD"/></td>

                    <td><Button size="sm" color="danger" onClick={()=>this.removeAsistenti(asistenti.id)} >Delete</Button></td>
                </tr>
            )

        return (

            <div>

                <div className="Apps">
                    <div className="Apps-header">

                        <h2>Welcome to Medication platform</h2>
                    </div>
                    <Chart chartData={this.state.chartData} location="Massachusetts" legendPosition="bottom"/>
                </div>

                <Container onSubmit={this.handleSubmit} >
                    {title}
                    <Form >
                        <FormGroup>
                            <Label for="title">Name: </Label>
                            <Input type="text" name="title" id="title" autoComplete="name" onChange={this.handleChange}/>
                        </FormGroup>

                        <FormGroup >
                            <Label for="location">Address: </Label>
                            <Input type="text" name="location" id="location" onChange={this.handleChange}/>
                        </FormGroup>
                        <FormGroup >
                            <Label for="title">Username: </Label>
                            <Input type="text" name="username" id="username" onChange={this.handleChange} />
                        </FormGroup>

                        <FormGroup >
                            <Label for="title">Password: </Label>
                            <Input type="text" name="password" id="password" onChange={this.handleChange}/>
                        </FormGroup >
                        <FormGroup>
                            <Label for="date">Birth Date: </Label>
                            <DatePicker selected={this.state.item.pacientdate} name="date" id="date" onChange={this.handleDateChange}/>

                        </FormGroup>
                        <FormGroup >
                            <Label for="title">Gen: </Label>
                            <select onChange={this.handleChange}>
                                <option>Female</option>
                                <option>Male</option>
                            </select>
                        </FormGroup>


                        <FormGroup >
                            <Label for="title">Rol: </Label>
                            <select onChange={this.handleChange}>
                                <option>Pacient</option>
                                <option>Asistent</option>
                            </select>
                        </FormGroup>
                        <FormGroup>
                            <Button color="primary" type="submit">Save</Button>
                            <Button color="secondary" tag={Link} to="/">Back</Button>
                        </FormGroup>




                    </Form>
                </Container>

                <Container>
                    <h3>Pacient List</h3>
                    <Table className="mt-lg-4">
                        <thead>
                        <tr>
                            <th width="20%">Name</th>
                            <th width="20%">Username</th>
                            <th width="20%">Password</th>
                            <th width="20%">Address</th>
                            <th width="20%">Gen</th>
                            <th width="20%">Data</th>
                            <th width="20%">Delete</th>
                            <th width="20%">Add medication plan</th>
                        </tr>
                        </thead>
                        <tbody>
                        {rows}
                        </tbody>
                    </Table>
                </Container>
                <Container>
                    <h3>Asistent List</h3>
                    <Table className="mt-lg-4">
                        <thead>
                        <tr>
                            <th width="20%">Name</th>
                            <th width="20%">Username</th>
                            <th width="20%">Password</th>
                            <th width="20%">Address</th>
                            <th width="20%">Gen</th>
                            <th width="20%">Data</th>
                            <th width="20%">Delete</th>
                        </tr>
                        </thead>

                        <tbody>
                        {rowss}
                        </tbody>


                    </Table>
                </Container>



                <div className="container">

                    <h2 > Timer </h2>
                    <div className="timer-container" className="text-center">
                        <div className="current-timer">{this.state.hours + ":" + this.state.minutes + ":" + this.state.seconds}</div>
                        <div className="timer-controls">
                            <button className="btn btn-success" onClick={this.handleTimerStart.bind(this)}>Start Timer</button>
                            <button className="btn btn-danger" onClick={this.handleTimerStop.bind(this)}>Stop Timer</button>
                        </div>
                    </div>
                </div>
            </div>


        )
    }
}
export default Doctor;