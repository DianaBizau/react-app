import React,{Component} from 'react';
import {Button, Container, Table} from "reactstrap";

class Pacient extends React.Component{
    state={
        timerStarted: false,
        timerStopped: true,
        hours: 0,
        minutes: 0,
        seconds: 0,
        captures: [],
        isLoading:true,
        Pacient:[],
        Medication:[]
    }
    async componentDidMount(){
        const response=await fetch('/App/getPacienti');
        const body = await response.json();
        this.setState({Pacient :body, isLoading: false});

        const responseM=await fetch('/medication/findAll');
        const bodyM = await responseM.json();
        this.setState({Medication :bodyM, isLoading: false});
    }
    handleTimerStop() {
        this.setState({timerStarted: false, timerStopped: true});
        clearInterval(this.timer);
    }
    handleTimerStart(e) {
        e.preventDefault();
        if(this.state.timerStopped) {
            this.timer = setInterval(() => {
                this.setState({timerStarted: true, timerStopped: false});
                if(this.state.timerStarted) {
                    if(this.state.seconds >= 60) {
                        this.setState((prevState) => ({ minutes: prevState.minutes + 1, seconds: 0}));
                    }
                    if(this.state.minutes >= 60) {
                        this.setState((prevState) => ({ hours: prevState.hours + 1, minutes: 0, seconds: 0}));

                    }
                    this.setState((prevState) => ({ seconds: prevState.seconds + 1 }));
                }

            }, 1000);
        }
    }

    render(){
        const{Medication}=this.state;

        const{Pacient, isLoading}=this.state;
        if(isLoading)
            return (<div>Loading ... </div>);
        let rowss=
            Medication.map(medication=>
                <tr key={medication.id}>
                    <td>{medication.name}</td>
                    <td>{medication.doza}</td>

                </tr>
            )
        return (

            <div>
                <Container>
                    <h3>Medication List</h3>
                    <Table className="mt-lg-4">
                        <thead>
                        <tr>
                            <th width="20%">Name</th>
                            <th width="20%">Doza</th>
                        </tr>
                        <td><Button size="sm" color="primary" >Download new Medication plan</Button> </td>
                        </thead>

                        <tbody>
                        {rowss}
                        </tbody>
                    </Table>
                </Container>

                <div className="container">

                    <h2 > Timer </h2>
                    <div className="timer-container" className="text-center">
                        <div className="current-timer">{this.state.hours + ":" + this.state.minutes + ":" + this.state.seconds}</div>
                        <div className="timer-controls">
                            <button className="btn btn-success" onClick={this.handleTimerStart.bind(this)}>Start Timer</button>
                            <button className="btn btn-danger" onClick={this.handleTimerStop.bind(this)}>Stop Timer</button>
                        </div>
                    </div>
                </div>

                <h2>
                    Medication:
                </h2>
                {
                    Medication.map(medication=>
                    <div>id={medication.id},{medication.name}</div>)
                }
                <h2>
                    Pacient:
                </h2>
                {
                    Pacient.map(pacient =>
                        <div>id={pacient.id},{pacient.name}</div>)
                }
            </div>
        )
    }

}
export default Pacient;