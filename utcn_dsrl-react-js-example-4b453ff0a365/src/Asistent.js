import React,{Component} from 'react';
import {Container, Table} from "reactstrap";

class Asistent extends React.Component{
    state={

        isLoading:true,
        Pacient:[],
        Medication:[]
    }
    async componentDidMount(){
        const response=await fetch('/App/getPacienti');
        const body = await response.json();
        this.setState({Pacient :body, isLoading: false});

        const responseM=await fetch('/medication/findAll');
        const bodyM = await responseM.json();
        this.setState({Medication :bodyM, isLoading: false});
    }
    render(){
        const{Medication}=this.state;

        const{Pacient, isLoading}=this.state;
        if(isLoading)
            return (<div>Loading ... </div>);
        let rowss=
            Medication.map(medication=>
                <tr key={medication.id}>
                    <td>{medication.name}</td>
                    <td>{medication.doza}</td>

                </tr>
            )
        return (

            <div>
                <Container>
                    <h3>Medication List</h3>
                    <Table className="mt-lg-4">
                        <thead>
                        <tr>
                            <th width="20%">Name</th>
                            <th width="20%">Doza</th>
                        </tr>
                        </thead>
                        <tbody>
                        {rowss}
                        </tbody>
                    </Table>
                </Container>
                <h2>
                    Medication:
                </h2>
                {
                    Medication.map(medication=>
                        <div>id={medication.id},{medication.name}</div>)
                }
                <h2>
                    Pacient:
                </h2>
                {
                    Pacient.map(pacient =>
                        <div>id={pacient.id},{pacient.name}</div>)
                }
            </div>
        )
    }

}
export default Asistent;