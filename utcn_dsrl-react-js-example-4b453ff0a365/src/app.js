import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import Persons from './person-data/person/persons'
import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import Doctor from './Doctor'
import Pacient from './Pacient'
import Asistent from './Asistent'
import Medication from'./Medication'

import Login from './login'
import  Register from './register'
import Apps from "./Apps";
let enums = require('./commons/constants/enums');

class App extends React.Component {


    render() {

        return (

            <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>
                        <Route path='/doctor' exact={true} component={Doctor}
                        />
                        <Route path='/pacienti' exact={true} component={Pacient}
                        />
                        <Route path='/caregivers' exact={true} component={Asistent}
                        />
                        <Route path='/medication' exact={true} component={Medication}
                        />
                        <Route path='/login' exact={true} component={Login}
                        />
                        <Route path='/register' exact={true} component={Register}
                        />
                        <Route path='/chart' exact={true} component={Apps}
                        />
                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />

                        <Route
                            exact
                            path='/doctor'
                            render={() => <Persons/>}
                        />

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
